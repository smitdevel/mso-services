﻿using System;
using System.ServiceProcess;
using log4net;

namespace MsoServices
{
    static class Program
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            log.Info("Main: Starting....");
            log.Info("Main: Creating new Program..");
            log.Info("Main: New Program created. Checking program running method - Console or Service...");
            if (Environment.UserInteractive)
            {
                MSOServiceHandler program = new MSOServiceHandler();
                log.Info("Main: MSOService for Delta starting in console mode...");
                program.OnStartPublic(args);
                log.Info("Main: MSOService for Delta starting in console mode... Started!");
                Console.WriteLine("Press any key to stop program");
                Console.Read();
                program.OnStopPublic();
                log.Info("Main: MSOService for Delta Stopped...");
            }
            else
            {
                log.Info("Main: MSOService for Delta starting in service mode...");
                ServiceBase[] ServiceToRun;
                ServiceToRun = new ServiceBase[]
                {
                new MSOServiceHandler()
                };
                ServiceBase.Run(ServiceToRun);
            }

            log.Info("That's it....");
        }
    }
}
