﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;
using MSOService;

namespace MsoServices
{
    partial class MSOServiceHandler : ServiceBase
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ServiceHost serviceHost = null;

        public MSOServiceHandler()
        {
            InitializeComponent();
        }
        
        public void OnStartPublic(string[] args)
        {
            OnStart(args);
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            base.OnStart(args);
            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            // Create a ServiceHost for the MSOServiceImpl type and 
            // provide the base address.
            serviceHost = new ServiceHost(typeof(MsoService));

            // Open the ServiceHostBase to create listeners and start 
            // listening for messages.
            serviceHost.Open();

            log.Info("OnStart: MSOService for Delta started...");
        }

        public void OnStopPublic()
        {
            OnStop();
        }
        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            base.OnStop();
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }
            log.Info("OnStop: MSOService for Delta stoped...");

        }

        public void OnContinuePublic()
        {
            OnContinue();
        }

        protected override void OnContinue()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            // Create a ServiceHost for the MSOServiceImpl type and 
            // provide the base address.
            serviceHost = new ServiceHost(typeof(MSOService.MsoService));

            // Open the ServiceHostBase to create listeners and start 
            // listening for messages.
            serviceHost.Open();
            log.Info("OnContinue: MSOService is continuing in working...");

        }
    }
}
