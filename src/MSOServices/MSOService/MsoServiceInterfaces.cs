﻿using System;
using System.ServiceModel;


namespace MSOService
{

    public class Constants
    {
        public const string Namespace = "http://webmedia.ee/mso";
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "MsoPortBinding", Namespace = Constants.Namespace)]

    [ServiceContract(Namespace = Constants.Namespace, Name = "MsoPortBinding")]
    public interface IMsoPortBinding
    {
        [OperationContract]
        msoPdfOutput convertToPdf(msoDocumentInput msoDocumentInput);

        [OperationContract]
        msoDocumentOutput replaceFormulas(msoDocumentAndFormulasInput msoDocumentAndFormulasInput);

        [OperationContract]
        msoDocumentAndPdfOutput replaceFormulasAndConvertToPdf(msoDocumentAndFormulasInput msoDocumentAndFormulasInput);

        [OperationContract]
        formulaOutput modifiedFormulas(msoDocumentInput msoDocumentInput);

        [OperationContract]
        pingOutput ping();

        [OperationContract]
        getSystemLoadOutput getSystemLoad();
    }

    [MessageContract]
    public class msoDocumentInput
    {
        [MessageBodyMember()]
        public String uid { get; set; }
        [MessageBodyMember()]
        public String idCode { get; set; }
        [MessageBodyMember()]
        public String fileName { get; set; }
        [MessageBodyMember()]
        public String fileType { get; set; }
        [MessageBodyMember()]
        public String fileEncoding { get; set; }
        [MessageBodyMember()]
        public Byte[] documentFile { get; set; }
    }

    [MessageContract]
    public class msoDocumentAndPdfOutput
    {
        [MessageBodyMember()]
        public Byte[] pdfFile { get; set; }
        [MessageBodyMember()]
        public Byte[] documentFile { get; set; }
    }

    [MessageContract]
    public class msoPdfOutput
    {
        [MessageBodyMember()]
        public Byte[] pdfFile { get; set; }
    }

    [MessageContract]
    public class msoDocumentOutput
    {
        [MessageBodyMember()]
        public Byte[] documentFile { get; set; }
    }

    [MessageContract]
    public class formulaOutput
    {
        [MessageBodyMember()]
        public formula[] formulas { get; set; }
    }

    [MessageContract]
    public class msoDocumentAndFormulasInput
    {
        [MessageBodyMember()]
        public String uid { get; set; }
        [MessageBodyMember()]
        public String idCode { get; set; }
        [MessageBodyMember()]
        public String fileType { get; set; }
        [MessageBodyMember()]
        public String fileEncoding { get; set; }
        [MessageBodyMember()]
        public Byte[] documentFile { get; set; }
        [MessageBodyMember()]
        public formula[] formulas { get; set; }
        [MessageBodyMember()]
        public Boolean dontSaveIfUnmodified { get; set; }
    }

    public class formula
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    [MessageContract]
    public class pingOutput
    {
        [MessageBodyMember()]
        public String text { get; set; }
    }

    public class MsoProcess
    {
        [MessageBodyMember()]
        public String processId { get; set; }
        [MessageBodyMember()]
        public String processName { get; set; }
        [MessageBodyMember()]
        public String processStarTime { get; set; }
        [MessageBodyMember()]
        public String processRunTime { get; set; }
        [MessageBodyMember()]
        public String processMainWindowTitle { get; set; }
    }
    [MessageContract]
    public class getSystemLoadOutput
    {
        [MessageBodyMember()]
        public String currentCpuUsage { get; set; }
        [MessageBodyMember()]
        public String availableRAM { get; set; }
        [MessageBodyMember()]
        public String currentCpuUsageUnit { get; set; }
        [MessageBodyMember()]
        public String availableRAMUnit { get; set; }
        [MessageBodyMember()]
        public String processorCount { get; set; }
        [MessageBodyMember()]
        public String memoryCount { get; set; }
        [MessageBodyMember()]
        public String osType { get; set; }
        [MessageBodyMember()]
        public String msoType { get; set; }
        [MessageBodyMember()]
        public MsoProcess[] msoProcessList { get; set; }

    }


}
