﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.IO;
using Microsoft.Win32;
using log4net;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Configuration;

namespace MSOService
{
    [ServiceBehavior(Namespace = Constants.Namespace)]


    public class MsoService : IMsoPortBinding
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static bool isFileDataLoggerActive = Convert.ToBoolean(ConfigurationManager.AppSettings["isFileLoggerActive"]);

        public msoPdfOutput convertToPdf(msoDocumentInput input)
        {
            log.Debug("Request: convertToPdf");
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => { return true; };

            log.Debug("File encoding: " + input.fileEncoding);
            log.Debug("File type: " + input.fileType);
            log.Debug("ID-code: " + input.idCode);
            log.Debug("UUID: " + input.uid);
            log.Debug("Input file: " + input.documentFile.ToString());
            log.Debug("Filename: " + input.fileName);

            string fileName = Utils.getGuid();
            string fileType = Utils.getFileTypeFromMimeType(input.fileType);
            msoPdfOutput response = new msoPdfOutput();

            if (fileType != null)
            {
                log.Debug("File type exists! Start convertion...");
                // Start stopwatch
                Stopwatch watch = new Stopwatch();
                watch.Start();

                DateTime now = DateTime.Now;

                // Write input request to disk
                string filenameTime = Utils.getTimestampToFilename(now);
                if (isFileDataLoggerActive)
                {
                    if (Utils.writeRequestToFile(filenameTime + "-convertToPdf" + "-1-" + fileName + "." + fileType, input.documentFile))
                    {
                        log.Debug("convertToPdf: Request file writed to log data dir succesfully!");
                    }
                }

                string uri = Utils.generateUri(fileType, fileName);
                log.Debug("Generate URI: " + uri);

                File.WriteAllBytes(uri, input.documentFile);

                bool pdfGenerateResult = MsoUtils.pdfGenerate(uri, input.fileEncoding, fileType);

                if (pdfGenerateResult)
                {
                    uri = Utils.generateUri("pdf", fileName);
                    log.Debug("Generate PDF file uri..." + uri);

                    try
                    {
                        watch.Stop();
                        if (isFileDataLoggerActive)
                        {
                            log.Debug("Write pdf file to log folder: ");
                            Utils.writeRequestToFile(filenameTime + "-convertToPdf" + "-2-" + watch.Elapsed.TotalSeconds.ToString() + "s-" + fileName + ".pdf", File.ReadAllBytes(uri));
                        }
                        response.pdfFile = File.ReadAllBytes(uri);
                    }
                    catch (Exception ex)
                    {
                        //EventLog.WriteEntry(LOGSOURCE, "Unable to open file with uri " + uri, EventLogEntryType.Error);
                        log.Error("Unable write response to pdf file! URI: " + uri + "; ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                    }

                    Utils.removeOldFile(uri);

                } else
                {
                    log.Warn("No PDF file generated!");
                }

                return response;
            }
            else
            {
                log.Debug("File type is missing! Can't convert to pdf. Return empty response.");
                return response;
            }
        }

        // method to return system performance data
        public getSystemLoadOutput getSystemLoad()
        {
            log.Debug("getSystemLoad: Start processing Request...");
            try
            {
                getSystemLoadOutput response = new getSystemLoadOutput();


                PerformanceCounter cpuCounter;
                PerformanceCounter ramCounter;

                cpuCounter = new PerformanceCounter();

                cpuCounter.CategoryName = "Processor";
                cpuCounter.CounterName = "% Processor Time";
                cpuCounter.InstanceName = "_Total";

                ramCounter = new PerformanceCounter("Memory", "Available MBytes");
                string availableRAM = ramCounter.NextValue().ToString();
                var memFree = new PerformanceCounter("Memory", "Available MBytes");


                String processorCount = Environment.ProcessorCount.ToString();
                response.processorCount = processorCount;
                log.Debug("getSystemLoad: Processor count: " + processorCount + " core");

                //dynamic firstValue = cpuCounter.NextValue();
                string firstValue = cpuCounter.NextValue().ToString();
                System.Threading.Thread.Sleep(100);
                //dynamic secondValue = cpuCounter.NextValue();
                string secondValue = cpuCounter.NextValue().ToString();

                response.currentCpuUsage = secondValue;
                response.currentCpuUsageUnit = "%";
                log.Debug("getSystemLoad: Current CPU usage: " + secondValue + "%");

                response.availableRAM = memFree.RawValue.ToString();
                response.availableRAMUnit = "MB";
                //log.Debug("getSystemLoad: Available RAM: " + memFree.RawValue.ToString() + "MB");
                log.Debug("getSystemLoad: Free RAM (MB): " + memFree.RawValue + "MB");


                String applicationBitType = Environment.Is64BitProcess ? "64bit App" : "32bit App";
                response.msoType = applicationBitType;
                log.Debug("getSystemLoad: Application bit version: " + applicationBitType);

                String machineName = Environment.MachineName;
                log.Debug("getSystemLoad: Environment machineName: " + machineName);

                String osSystem = Environment.OSVersion.ToString();
                log.Debug("getSystemLoad: Environment OSVersion: " + osSystem);

                String osBitType = Environment.Is64BitOperatingSystem ? "64bit OS" : "32bit OS";
                log.Debug("getSystemLoad: Operation system bit version: " + osBitType);

                String envVersion = Environment.Version.ToString();
                log.Debug("getSystemLoad: Environment version: " + envVersion);

                var reg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion");
                string productName = (string)reg.GetValue("ProductName");

                log.Debug("getSystemLoad: ----------------------------");
                log.Debug("getSystemLoad: Operation System Information");
                log.Debug("getSystemLoad: ----------------------------");
                log.Debug("getSystemLoad: Product Name = " + productName);
                //log.Debug("getSystemLoad: Name = " + OSInfo.Name);
                log.Debug("getSystemLoad: Edition = " + OSInfo.Edition);
                log.Debug("getSystemLoad: Service Pack = " + OSInfo.ServicePack);
                log.Debug("getSystemLoad: Version = " + OSInfo.VersionString);
                log.Debug("getSystemLoad: Bits = " + OSInfo.Bits);

                log.Debug("getSystemLoad: ----------------------------");

                Process[] myProcs = Process.GetProcesses();
                var sorted = myProcs.OrderBy(p => p.UserProcessorTime);

                List<MsoProcess> msProtessList = new List<MsoProcess>();
                foreach (Process process in myProcs)
                {
                    string processName = process.ProcessName.ToLower();
                    if (processName == MsoUtils.PROCESS_EXCEL || processName == MsoUtils.PROCESS_WORD) {
                        MsoProcess ms = new MsoProcess();
                        ms.processId = process.Id.ToString();
                        ms.processName = processName;
                        try
                        {
                            DateTime startTime = process.StartTime;
                            DateTime now = DateTime.Now;
                            TimeSpan processTime = now - startTime;
                            ms.processStarTime = Utils.dateTimeToString(startTime);
                            ms.processRunTime = Utils.convertTimeSpanToHumanRedable(processTime);
                        }
                        catch (Exception ex)
                        {
                            ms.processStarTime = "[ERROR]! " + ex.Message;
                            ms.processRunTime = "[ERROR]! " + ex.Message;
                            log.Error(ms.processStarTime + "\r\n" + ex.StackTrace);
                        }

                        try
                        {
                            ms.processMainWindowTitle = process.MainWindowTitle;

                        } catch(Exception ex)
                        {
                            ms.processMainWindowTitle = "[ERROR]! " + ex.Message;
                            log.Error(ms.processMainWindowTitle + "\r\n" + ex.StackTrace);
                        }
                        log.Debug("Process processId: " + ms.processId);
                        log.Debug("Process processName: " + ms.processName);
                        log.Debug("Process processStarTime: " + ms.processStarTime);
                        log.Debug("Process processRunTime: " + ms.processRunTime);
                        log.Debug("process.MainWindowTitle: " + ms.processMainWindowTitle);

                        msProtessList.Add(ms);
                    }
                }

                MsoProcess[] msoProcessList = new MsoProcess[msProtessList.Count];
                int i = 0;
                foreach (MsoProcess msPro in msProtessList)
                {
                    msoProcessList[i++] = msPro;
                }
                response.msoProcessList = msoProcessList;
                //runPerformanceStat();
                log.Debug("getSystemLoad: Return resposne...");
                return response;
            }
            catch (Exception ex)
            {
                
                log.Info("getSystemLoad: Error in getSystemLoad() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                return null;
            }
        }

        public formulaOutput modifiedFormulas(msoDocumentInput input)
        {
            log.Debug("Request: modifiedFormulas");
            string fileName = Utils.getGuid();
            log.Debug("Filename: " + fileName);
            string fileType = Utils.getFileTypeFromMimeType(input.fileType);
            log.Debug("Filetype: " + fileType);
            string uri = Utils.generateUri(fileType, fileName);
            log.Debug("URI: " + uri);

            DateTime now = DateTime.Now;
            //Stopwatch watch = new Stopwatch();
            //watch.Start();
            string filenameTime = Utils.getTimestampToFilename(now);
            try
            {
                if (isFileDataLoggerActive)
                {
                    // Write input request to disk
                    Utils.writeRequestToFile(filenameTime + "-modifiedFormulas" + "-1-" + fileName + "." + fileType, input.documentFile);
                }

                File.WriteAllBytes(uri, input.documentFile);
            }
            catch (Exception ex)
            {
                //EventLog.WriteEntry(LOGSOURCE, "Unable to write file with uri " + uri, EventLogEntryType.Error);
                log.Error("modifiedFormulas: Unable to write file with uri " + uri + "; ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());
            }

            log.Debug("Start finding formula keys...");
            formula[] formulas = MsoUtils.findKeys(uri);

            formulaOutput response = new formulaOutput();
            response.formulas = formulas;

            Utils.removeOldFile(uri);

            return response;
        }

        // method to return service availability info
        public pingOutput ping()
        {
            log.Debug("ping: Start processing Request...");

            pingOutput response = new pingOutput();
            response.text = "pong";

            log.Debug("ping: Returning response...");
            return response;
        }

        public msoDocumentOutput replaceFormulas(msoDocumentAndFormulasInput input)
        {
            log.Debug("Request: replaceFormulas...");

            string fileName = Utils.getGuid();
            log.Debug("Filename: " + fileName);
            string fileType = Utils.getFileTypeFromMimeType(input.fileType);
            log.Debug("Filetype: " + fileType);
            string uri = Utils.generateUri(fileType, fileName);
            log.Debug("URI: " + uri);

            formula[] formulaArray = input.formulas;
            if (formulaArray == null)
            {
                log.Debug("Request formulas array is empty/NULL. Nothing to replace...");
            }
            // get timestamp format for filename
            DateTime now = DateTime.Now;
            string filenameTime = Utils.getTimestampToFilename(now);
            Stopwatch watch = new Stopwatch();
            watch.Start();
            try
            {
                if (isFileDataLoggerActive)
                {
                    // Write input request to disk
                    Utils.writeRequestToFile(filenameTime + "-replaceFormulas" + "-1-" + fileName + "." + fileType, input.documentFile);
                }

                File.WriteAllBytes(uri, input.documentFile);
            }
            catch (Exception ex)
            {
                //EventLog.WriteEntry(LOGSOURCE, "Unable to write file with uri " + uri, EventLogEntryType.Error);
                log.Error("Unable to write file with uri " + uri + "; ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());

            }

            // find and replace formulas
            string filePath = MsoUtils.doSearchAndReplaceInWord(uri, fileType, formulaArray, input.dontSaveIfUnmodified);

            Utils.removeOldFile(uri);

            msoDocumentOutput response = new msoDocumentOutput();
            if (filePath != null)
            {
                try
                {
                    watch.Stop();
                    if (isFileDataLoggerActive)
                    {
                        Utils.writeRequestToFile(filenameTime + "-replaceFormulas" + "-2-" + watch.Elapsed.TotalSeconds.ToString() + "s-" + fileName + "." + fileType, File.ReadAllBytes(filePath));
                    }

                    response.documentFile = File.ReadAllBytes(filePath);

                    Utils.removeOldFile(filePath);
                }
                catch (Exception ex)
                {
                    //EventLog.WriteEntry(LOGSOURCE, "Unable to open file with uri " + uri, EventLogEntryType.Error);
                    log.Error("Unable to send file to response with uri " + uri + "; ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                }
            }

            return response;
        }

        public msoDocumentAndPdfOutput replaceFormulasAndConvertToPdf(msoDocumentAndFormulasInput input)
        {
            log.Debug("replaceFormulasAndConvertToPdf: Starting proccess request...");

            string fileName = Utils.getGuid();
            string fileType = Utils.getFileTypeFromMimeType(input.fileType);
            string uri = Utils.generateUri(fileType, fileName);
            formula[] formulaArray = input.formulas;

            DateTime now = DateTime.Now;

            // get timestamp format for filename
            string filenameTime = Utils.getTimestampToFilename(now);
            Stopwatch watch = new Stopwatch();
            watch.Start();

            try
            {
                if (isFileDataLoggerActive)
                {
                    // Write input request to disk
                    Utils.writeRequestToFile(filenameTime + "-replaceFormulasAndConvertToPdf" + "-1-" + fileName + "." + fileType, input.documentFile);
                }

                File.WriteAllBytes(uri, input.documentFile);
            }
            catch (Exception ex)
            {
                //EventLog.WriteEntry(LOGSOURCE, "Unable to write file with uri " + uri, EventLogEntryType.Error);
                log.Error("replaceFormulasAndConvertToPdf: Unable to write file with uri " + uri + "; ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());
            }

            // find and replace formulas
            string filePath = MsoUtils.doSearchAndReplaceInWord(uri, fileType, formulaArray, false);

            Utils.removeOldFile(uri);

            bool pdfCenerateResult = MsoUtils.pdfGenerate(filePath, input.fileEncoding, fileType);

            // Return pdf to webservice
            uri = Utils.generateUri("pdf", fileName);
            uri = uri.Replace(".pdf", "-temp.pdf");
            msoDocumentAndPdfOutput response = new msoDocumentAndPdfOutput();
            try
            {
                // Write response to disk
                watch.Stop();
                if (isFileDataLoggerActive)
                {
                    Utils.writeRequestToFile(filenameTime + "-replaceFormulasAndConvertToPdf" + "-2-1-" + watch.Elapsed.TotalSeconds.ToString() + "s" + fileName + ".pdf", File.ReadAllBytes(uri));
                    Utils.writeRequestToFile(filenameTime + "-replaceFormulasAndConvertToPdf" + "-2-2-" + watch.Elapsed.TotalSeconds.ToString() + "s" + fileName + "." + fileType, File.ReadAllBytes(filePath));
                }

                response.pdfFile = File.ReadAllBytes(uri);
                response.documentFile = File.ReadAllBytes(filePath);
            }
            catch (Exception ex)
            {
                //EventLog.WriteEntry(LOGSOURCE, "Unable to open file with uri " + uri, EventLogEntryType.Error);
                log.Error("replaceFormulasAndConvertToPdf: Unable to send file to response with uri " + uri + "; ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());
            }

            Utils.removeOldFile(filePath);
            Utils.removeOldFile(uri);

            return response;
        }
    }
}