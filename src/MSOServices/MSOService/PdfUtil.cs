﻿using System;
using System.IO;
using System.Diagnostics;
using Microsoft.Office.Interop.Word;
using WORD = Microsoft.Office.Interop.Word;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;
using EXCEL = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using log4net;

namespace MSOService
{
    public class PdfUtil
    {
        private static ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private const string PROCESS_WORD = "winword";
        private const string PROCESS_EXCEL = "excel";
        public static object oMissing = Missing.Value;
        private static bool removeEmptyFieldsParam = true;

        /**
         * Method that generates pdf from document file
         * input - String path to file
         * tpye - String file MIME type
         */
        public static bool generatePdfWord(string path, string encoding, bool findEncoding)
        {
            Utils.serverCertificateValidation();

            log.Debug("Defining WORD Application..");
            WORD._Application word = null;

            log.Debug("Defining WORD Document..");
            _Document doc = null;

            log.Debug("New File: " + path);
            FileInfo documentFile = new FileInfo(path);
            object filename = documentFile.FullName;
            string extension = documentFile.Extension;

            List<Process> processes = Utils.GetProcesses(PROCESS_WORD);
            bool exception = false;

            try
            {
                log.Debug("Get new Word Application...");
                word = new WORD.Application();

                word = MsoUtils.setWordAppWordParams(word);

                // Remove old processes
                Utils.FilterProcesses(processes, PROCESS_WORD);

                word.DisplayAlerts = WdAlertLevel.wdAlertsNone;
                word.Visible = false;
                word.ScreenUpdating = false;
                word.Options.UpdateLinksAtOpen = false;

                Encoding enc = null;
                if (findEncoding)
                {
                    enc = Utils.GetEncoding(path);
                    if (enc == null)
                    {
                        log.Debug("Encoding (enc): NULL");
                    }
                    else
                    {
                        log.Debug("Encoding (enc): " + enc.ToString());
                    }

                    Encoding enc1 = CharsetEncoding.DetectTextFileEncoding(path);
                    //Encoding enc = Utils.findFileEncoding(path, extension);

                }

                Encoding encodingValue = encoding != null ? Encoding.GetEncoding(encoding) : enc != null ? enc : null;
                log.Debug("Encoding value : " + encodingValue.ToString());

                object msoEnc = MsoUtils.MSOEncoding(encoding, enc);

                log.Debug("Check HTML content...");
                Utils.checkHtmlContent(path, extension, encodingValue);

                doc = MsoUtils.openWordDocument(word, doc, filename, msoEnc);

                if (doc == null) throw new Exception("MSO was unable to open WORD Document for PDF generation...");

                // On printing, Word updates all fields in headers and footers, even though we set UpdateFieldsAtPrint = false
                List<WORD.Field> foundFields = Utils.findFields(doc);
                log.Debug("generatePdfWord: Found word Fields count: " + foundFields.Count);

                Utils.fixFieldCodes(foundFields);
                foreach (WORD.Field field in foundFields)
                {
                    string fieldCode;
                    try
                    {
                        fieldCode = field.Code.Text;
                    }
                    catch (COMException ex)
                    {
                        // Somehow two fields in a file got into an state, so that nothing can be done with them, all programmatical actions throw exception.
                        // If "Update field" is manually executed on these fields in Word GUI, then problem dissapears.
                        // Problem cannot be reproduced in doSearchAndReplaceInWord or findKeys, it happens only here.
                        log.Error("ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString() + "\r\n"
                            + "MESSAGE: Somehow two fields in a file got into an state, so that nothing can be done with them, all programmatical actions throw exception."
                            + "If \"Update field\" is manually executed on these fields in Word GUI, then problem dissapears."
                            + "Problem cannot be reproduced in doSearchAndReplaceInWord or findKeys, it happens only here");
                        continue;
                    }
                    string deltaFieldCode = Utils.getDeltaFieldCode(fieldCode);
                    if (deltaFieldCode == null)
                    {
                        continue;
                    }

                    // If non-DOCPROPERTY field has empty value, then its value is replaced by Word with "Error! Bookmark not defined." when print or print preview is executed.
                    // So delete fields with empty values.

                    //Console.WriteLine("FieldCode: [" + fieldCode + "]; FieldValue: [" + field.Result.Text + "]");
                    log.Debug("FieldCode: [" + fieldCode + "]; FieldValue: [" + field.Result.Text + "]");

                    if (field.Result.Text == null || field.Result.Text.Length == 0)
                    {
                        field.Delete();
                    }
                    else
                    {
                        if (removeEmptyFieldsParam)
                        {
                            // Delete empty field
                            Match match = Regex.Match(field.Result.Text, @"{(.*)}$", RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                // Finally, we get the Group value and display it.
                                string key = match.Groups[1].Value;
                                //Console.WriteLine("Found empty field: " + key);
                                log.Debug("Found empty field: " + key + ". Delete it...");

                                field.Delete();
                            }
                        }
                    }
                }

                object outputFileName = documentFile.FullName.Replace(extension, ".pdf");
                object fileFormat = WdSaveFormat.wdFormatPDF;
                string outputFileNameString = outputFileName.ToString();

                word.Options.UpdateFieldsAtPrint = false;
                word.Options.UpdateFieldsWithTrackedChangesAtPrint = false;
                word.Options.UpdateLinksAtPrint = false;
                doc.ExportAsFixedFormat(outputFileNameString, WdExportFormat.wdExportFormatPDF, false, WdExportOptimizeFor.wdExportOptimizeForPrint,
                    WdExportRange.wdExportAllDocument, 1, 1, WdExportItem.wdExportDocumentContent, true, true,
                    WdExportCreateBookmarks.wdExportCreateHeadingBookmarks, true, true, true, ref oMissing);

                return true;
            }
            catch (Exception ex)
            {
                //EventLog.WriteEntry(LOGSOURCE, "Error in generatePdf() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                log.Error("Error in generatePdf() method: Error Message: [" + ex.Message + "], Stacktrace: " + ex.StackTrace.ToString());

                exception = true;
                throw;
            }
            finally
            {
                if (doc != null)
                {
                    try
                    {
                        doc.Close(WdSaveOptions.wdDoNotSaveChanges, ref oMissing, ref oMissing);
                    }
                    catch (Exception ex)
                    {
                        //EventLog.WriteEntry(LOGSOURCE, "Error in generatePdf() method. " + ex.Message+ " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                        log.Error("Error in generatePdf() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                        exception = true;
                    }
                    doc = null;
                }

                if (word != null)
                {
                    try
                    {
                        word.Quit(WdSaveOptions.wdDoNotSaveChanges, ref oMissing, ref oMissing);
                    }
                    catch (Exception ex)
                    {
                        //EventLog.WriteEntry(LOGSOURCE, "Error in generatePdf() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                        log.Error("Error in generatePdf() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString());

                        exception = true;
                    }
                    word = null;
                }

                if (exception)
                {
                    Utils.KillProcesses(processes);
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        /*
        * method that generates pdf from document file
        * input - String path to file
        * tpye - String file MIME type
        * */
        public static bool generatePdfExel(string path, string fileType)
        {
            log.Debug("Request: generatePdfExel...");

            Utils.serverCertificateValidation();

            Utils.checkSystemFolders();

            if(Utils.checkPrinter() == false)
            {
                log.Error("CAN'T CONVERT EXCEL TO PDF! DEFAULT PRINTER DOES NOT EXIST!");
                return false;
            }
            EXCEL._Application excel = null;
            _Workbook workbook = null;


            FileInfo documentFile = new FileInfo(path);
            string fileNameString = documentFile.FullName;
            log.Debug("generatePdfExel(): Full filename: " + fileNameString);

            string fileName = Path.GetFileNameWithoutExtension(documentFile.FullName);
            log.Debug("generatePdfExel(): Simple filename: " + fileName);

            object fileNameObject = documentFile.FullName;
            List<Process> processes = Utils.GetProcesses(PROCESS_EXCEL);
            bool exception = false;

            try
            {
                excel = new EXCEL.Application();
                Utils.FilterProcesses(processes, PROCESS_EXCEL);
                excel.DefaultSaveFormat = XlFileFormat.xlOpenXMLWorkbook;
                excel.DisplayAlerts = false;
                excel.Visible = false;
                excel.ScreenUpdating = false;
                excel.AskToUpdateLinks = false;

                workbook = excel.Workbooks.Open(fileNameString,
                    oMissing, false, oMissing, oMissing,
                    oMissing, true, oMissing, oMissing,
                    oMissing, false, oMissing, false,
                    false, XlCorruptLoad.xlNormalLoad);

                workbook.Activate();

                string uri = Utils.generateUri("pdf", fileName);
                log.Debug("generatePdfExel(): URI: " + uri);

                workbook.ExportAsFixedFormat(XlFixedFormatType.xlTypePDF, uri, XlFixedFormatQuality.xlQualityStandard,
                    true, false, oMissing, oMissing, false, oMissing);

                return true;
            }
            catch (Exception ex)
            {
                log.Error("generatePdfExel(): ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());

                exception = true;
                throw;
            }
            finally
            {
                if (workbook != null)
                {
                    try
                    {
                        workbook.Close(false, oMissing, oMissing);
                    }
                    catch (Exception ex)
                    {
                        log.Error("generatePdfExel: ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());

                        exception = true;
                    }
                    workbook = null;
                }
                if (excel != null)
                {
                    try
                    {
                        excel.Quit();
                    }
                    catch (Exception ex)
                    {
                        log.Error("generatePdfExel: ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());

                        exception = true;
                    }
                    excel = null;
                }

                if (exception)
                {
                    Utils.KillProcesses(processes);
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

     }
}