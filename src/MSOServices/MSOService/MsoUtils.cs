﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using Microsoft.Office.Core;
using System.Reflection;
using Microsoft.Office.Interop.Word;
using WORD = Microsoft.Office.Interop.Word;

using log4net;
using System.Net;

namespace MSOService
{
    public class MsoUtils
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public const string PROCESS_WORD = "winword";
        public const string PROCESS_EXCEL = "excel";
        public static object oMissing = Missing.Value;
        //private static bool removeEmptyFieldsParam = true;

        /*
         * Method that finds keys from file
         * */
        public static formula[] findKeys(String path)
        {
            Utils.serverCertificateValidation();

            log.Debug("Defining WORD Application..");
            WORD._Application word = null;

            log.Debug("Defining WORD Document..");
            WORD._Document doc = null;


            List<formula> formulaList = new List<formula>();
            FileInfo documentFile = new FileInfo(path);
            Object filename = (Object)documentFile.FullName;
            List<Process> processes = Utils.GetProcesses(PROCESS_WORD);
            bool exception = false;

            try
            {
                word = new WORD.Application();

                word = MsoUtils.setWordAppWordParams(word);

                Utils.FilterProcesses(processes, PROCESS_WORD);

                word.DisplayAlerts = WdAlertLevel.wdAlertsNone;
                word.Visible = false;
                word.ScreenUpdating = false;
                word.Options.UpdateLinksAtOpen = false;

                doc = word.Documents.OpenNoRepairDialog(ref filename, false, false, false, ref oMissing, ref oMissing, true,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing, false,
                    false, ref oMissing, true, ref oMissing);
                doc.Activate();

                List<WORD.Field> foundFields = Utils.findFields(doc);
                Utils.fixFieldCodes(foundFields);
                foreach (WORD.Field field in foundFields)
                {
                    string deltaFieldCode = Utils.getDeltaFieldCode(field.Code.Text);
                    if (deltaFieldCode == null)
                    {
                        continue;
                    }
                    string deltaFieldResult = Utils.getDeltaFieldResult(deltaFieldCode, field.Result.Text);
                    if (deltaFieldResult.Trim().Equals("Error! Reference source not found."))
                    {
                        continue;
                    }

                    formula formula = new formula();
                    formula.key = Utils.getFieldCodeSubstitutedBackToDelta(deltaFieldCode);
                    formula.value = deltaFieldResult;
                    formulaList.Add(formula);
                }

                formula[] formulad = formulaList.ToArray();
                return formulad;
            }
            catch (Exception ex)
            {
                //EventLog.WriteEntry(LOGSOURCE, "Error in findKeys() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                log.Error("Error in findKeys() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                exception = true;
                throw;
            }
            finally
            {
                if (doc != null)
                {
                    try
                    {
                        doc.Close(WdSaveOptions.wdDoNotSaveChanges, ref oMissing, ref oMissing);
                    }
                    catch (Exception ex)
                    {
                        //EventLog.WriteEntry(LOGSOURCE, "Error in findKeys() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                        log.Error("Error in findKeys() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                        exception = true;
                    }
                    doc = null;
                }

                if (word != null)
                {
                    try
                    {
                        word.Quit(WdSaveOptions.wdDoNotSaveChanges, ref oMissing, ref oMissing);
                    }
                    catch (Exception ex)
                    {
                        //EventLog.WriteEntry(LOGSOURCE, "Error in findKeys() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                        log.Error("Error in findKeys() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                        exception = true;
                    }
                    word = null;
                }

                if (exception)
                {
                    Utils.KillProcesses(processes);
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }


        /*
         * Method that works with word .dot document, replaces all blanks and returns new word file or pdf fail if needed
         * input is file name and map of key value pairs
         * output is report about success
         * io output is docx document file generated after proccess
         * */
        public static string doSearchAndReplaceInWord(string path, string type, formula[] formulas, bool checkModified)
        {
            Utils.serverCertificateValidation();

            _Application word = null;
            _Document doc = null;


            FileInfo documentFile = new FileInfo(path);
            object filename = documentFile.FullName;
            List<Process> processes = Utils.GetProcesses(PROCESS_WORD);
            bool exception = false;

            try
            {
                word = new Application();

                word = setWordAppWordParams(word);

                Utils.FilterProcesses(processes, PROCESS_WORD);
                word.DisplayAlerts = WdAlertLevel.wdAlertsNone;
                word.Visible = false;
                word.ScreenUpdating = false;
                word.Options.UpdateLinksAtOpen = false;
                log.Debug("Opening document...");
                doc = word.Documents.OpenNoRepairDialog(ref filename, false, false, false, ref oMissing, ref oMissing, true,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing, false, false, ref oMissing, true, ref oMissing);
                doc.Activate();

                doc = setWordDocWordParams(doc);

                bool finalize = false;
                Dictionary<string, string> formulasDictionary = new Dictionary<string, string>();
                if(formulas != null)
                {
                    log.Debug("Formulas contains fileds. Size: " + formulas.Length);
                }

                int i = 0;

                foreach (formula formula in formulas)
                {
                    i++;
                    if (formula == null)
                    {
                        log.Error(i + ") Formula is null! Continue...");
                        continue;
                        //throw new Exception("Formula must have a key with length");
                    }

                    string formulaKey = formula.key;
                    string formulaValue = formula.value;
                    
                    if(formulaKey == null)
                    {
                        log.Error(i + ") Formula must have a key! Continue...");
                        continue;
                    }

                    log.Debug(i + "Formula key: " + formulaKey + " ==> value: " + formulaValue);
                    if ((string.Compare("$FINALIZE", formulaKey)) == 0
                        || (string.Compare("FINALIZE", formulaKey)) == 0) // TODO: REMOVE THIS LINE IF ALL DELTAS HAVE UPGRADED TO 3.6.30 OR NEWER
                    {
                        log.Debug(i + ")Found FINALIZE formula key!");
                        finalize = true;
                    }
                    else
                    {
                        log.Debug(i + ") Add formula to dictionary...");
                        formulasDictionary.Add(Utils.getFieldCodeSubstitutedFromDelta(formulaKey), formulaValue);
                    }
                }

                log.Debug("Input formulas check done! Find fields from document...");
                //Update all the fields    
                List<Field> foundFields = Utils.findFields(doc);

                if(foundFields != null)
                {
                    log.Debug("Found " + foundFields.Count + " fields from document.");
                }
                log.Debug("Fix document fields...");
                Utils.fixFieldCodes(foundFields);


                bool documentChanged = false;
                foreach (Field field in foundFields)
                {
                    string deltaFieldCode;
                    try
                    {
                        log.Debug("Get fieldCodeText...");
                        deltaFieldCode = Utils.getDeltaFieldCode(field.Code.Text);
                    }
                    catch
                    {
                        log.Warn("fieldCodeText error... Continue.");
                        continue;
                    }

                    if (deltaFieldCode == null)
                    {
                        log.Debug("FieldCodeText is NULL! Continue.");
                        continue;
                    }

                    string fieldValue = null;
                    if (formulasDictionary.ContainsKey(deltaFieldCode))
                    {
                        log.Debug("Found match! Formulas dictionary contain FieldCodeText!");
                        fieldValue = formulasDictionary[deltaFieldCode];
                    }

                    if (fieldValue == null || fieldValue.Length == 0)
                    {
                        log.Debug("FieldValue is NULL! Change field back to DELTA format...");
                        fieldValue = "{" + Utils.getFieldCodeSubstitutedBackToDelta(deltaFieldCode) + "}";
                    }

                    // ^^ It's good, that we always change all field values TO NON-EMPTY.
                    // Because if non-DOCPROPERTY field has empty value, then its value is replaced by Word with "Error! Bookmark not defined." when user executes print or print preview.

                    string currentValue = field.Result.Text;
                    if (fieldValue != currentValue)
                    {
                        log.Debug("Field values is different than current value. Change document!");
                        documentChanged = true;
                    }
                    if (currentValue != null && currentValue.Length > 0)
                    {
                        field.Result.Text = fieldValue;
                    }
                    else
                    {
                        // If current field value is empty, then setting a new value - Word does not update field value, but writes new value next to field as text
                        // We must work around this quirk

                        string suffix = field.Code.Text.Substring(field.Code.Text.IndexOf(deltaFieldCode) + deltaFieldCode.Length);

                        // We could add a document property with random name, but removing it didn't work on the first try :(
                        string docPropCode = "delta_" + Utils.getFieldCodeSubstitutedBackToDelta(deltaFieldCode);

                        object oDocCustomProps = doc.CustomDocumentProperties;
                        Type typeDocCustomProps = oDocCustomProps.GetType();
                        try
                        {
                            // Set property - works if property exists
                            typeDocCustomProps.InvokeMember("Item",
                                BindingFlags.Default | BindingFlags.SetProperty,
                                null,
                                oDocCustomProps,
                                new object[] { docPropCode, fieldValue });
                        }
                        catch (Exception ex)
                        {
                            log.Warn("doSearchAndReplaceInWord: WARN: Property does not exist, add it..." + ex.Message + "; Stack: " + ex.StackTrace.ToString());
                            // Property does not exist, add it
                            typeDocCustomProps.InvokeMember("Add",
                                BindingFlags.Default | BindingFlags.InvokeMethod,
                                null,
                                oDocCustomProps,
                                new object[] { docPropCode, false, MsoDocProperties.msoPropertyTypeString, fieldValue });
                        }

                        field.Code.Text = " DOCPROPERTY  " + docPropCode + suffix;
                        field.Update();
                        field.Code.Text = " delta_" + deltaFieldCode + suffix;

                        field.Result.Text = fieldValue;

                        // Background information:
                        // Normally user cannot set field value to empty, because deleting field value in Word GUI deletes the field entirely
                        // But field value can be set empty programmatically by users
                        // And previous versions of MSO2 did set some field values to empty, that's why there exist some Word files in Delta with empty field values
                    }
                }

                if (finalize)
                {
                    foreach (WORD.Range rngStory in doc.StoryRanges)
                    {
                        try
                        {
                            rngStory.Select();
                            documentChanged |= Utils.insertExtraSpaces(doc);
                            rngStory.Select();
                            documentChanged |= Utils.finalizeSearch(doc);
                            documentChanged |= Utils.deleteUnusedFields(rngStory.Fields);

                            if (rngStory.ShapeRange.Count > 0)
                            {
                                foreach (WORD.Shape oShp in rngStory.ShapeRange)
                                {
                                    if (oShp.TextFrame.HasText < 0)
                                    {
                                        oShp.Select();
                                        documentChanged |= Utils.insertExtraSpaces(doc);
                                        oShp.Select();
                                        documentChanged |= Utils.finalizeSearch(doc);
                                        documentChanged |= Utils.deleteUnusedFields(oShp.TextFrame.ContainingRange.Fields);
                                    }
                                }
                            }
                        }
                        catch { }
                    }
                }
                if (checkModified && !documentChanged)
                {
                    return null;
                }

                string fileType = null;
                string fileTypeNew = null;
                object fileFormat = null;

                if (string.Compare(type, "doc") == 0)
                {
                    fileType = ".doc";
                    fileTypeNew = "-temp.doc";
                    fileFormat = WdSaveFormat.wdFormatDocument97;
                }
                if (string.Compare(type, "dotx") == 0)
                {
                    fileType = ".dotx";
                    fileTypeNew = "-temp.docx";
                    fileFormat = WdSaveFormat.wdFormatXMLDocument;
                }
                if (string.Compare(type, "docx") == 0)
                {
                    fileType = ".docx";
                    fileTypeNew = "-temp.docx";
                    fileFormat = WdSaveFormat.wdFormatXMLDocument;
                }

                // Save the new file
                object outputFileName = documentFile.FullName.Replace(fileType, fileTypeNew);

                doc.SaveAs(ref outputFileName,
                    ref fileFormat, ref oMissing, ref oMissing,
                    false, ref oMissing, ref oMissing, ref oMissing,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing);
                return outputFileName.ToString();
            }
            catch (Exception ex)
            {
                //EventLog.WriteEntry(LOGSOURCE, "Error in doSearchAndReplaceInWord() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                log.Error("doSearchAndReplaceInWord: ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                exception = true;
                throw;
            }
            finally
            {
                if (doc != null)
                {
                    try
                    {
                        doc.Close(WdSaveOptions.wdDoNotSaveChanges, ref oMissing, ref oMissing);
                    }
                    catch (Exception ex)
                    {
                        //EventLog.WriteEntry(LOGSOURCE, "Error in doSearchAndReplaceInWord() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                        log.Error("doSearchAndReplaceInWord: ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                        exception = true;
                    }
                    doc = null;
                }

                if (word != null)
                {
                    try
                    {
                        word.Quit(WdSaveOptions.wdDoNotSaveChanges, ref oMissing, ref oMissing);
                    }
                    catch (Exception ex)
                    {
                        //EventLog.WriteEntry(LOGSOURCE, "Error in doSearchAndReplaceInWord() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                        log.Error("doSearchAndReplaceInWord: ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                        exception = true;
                    }
                    word = null;
                }

                if (exception)
                {
                    Utils.KillProcesses(processes);
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public static WORD._Application setWordAppWordParams(WORD._Application word)
        {
            log.Debug("Set WORD Application ReadingLayout option to FALSE...");
            word = setWordAppReadingLayout(word);
            log.Debug("Set WORD Application ReadingLayaoutAllowEditing to TRUE");
            word = setWordAppReadingLayoutAllowEditing(word);
            return word;
        }

        public static WORD._Application setWordAppReadingLayout(WORD._Application word)
        {
            try
            {
                if (word.ActiveWindow.Active)
                {
                    word.ActiveWindow.View.ReadingLayout = false;
                } else
                {
                    log.Warn("WORD's Windows is not Active!");
                }
            }
            catch (Exception ex)
            {
                log.Error("Can't set WORD ReadingLayout to false (in writable mode)!..." + ex.Message);
            }

            return word;
        }

        public static WORD._Application setWordAppReadingLayoutAllowEditing(WORD._Application word)
        {
            try
            {
                if (word.ActiveWindow.Active)
                {
                    word.ActiveWindow.View.ReadingLayoutAllowEditing = true;
                } else
                {
                    log.Warn("WORD's Windows is not Active!");
                }

            }
            catch (Exception ex)
            {
                log.Error("Can't set WORD ReadingLayoutAllowEditing to true (in writable mode)!..." + ex.Message);
            }

            return word;
        }

        public static WORD._Document setWordDocWordParams(WORD._Document word)
        {
            log.Debug("Set WORD Application ReadingLayout option to FALSE...");
            word = setWordDocReadingLayout(word);
            log.Debug("Set WORD Application ReadingLayaoutAllowEditing to TRUE");
            word = setWordDocReadingLayoutAllowEditing(word);
            return word;
        }

        public static WORD._Document setWordDocReadingLayout(WORD._Document word)
        {
            try
            {
                if (word.ActiveWindow.Active)
                {
                    word.ActiveWindow.View.ReadingLayout = false;
                }
                else
                {
                    log.Warn("WORD's Windows is not Active!");
                }
            }
            catch (Exception ex)
            {
                log.Error("Can't set WORD ReadingLayout to false (in writable mode)!..." + ex.Message);
            }

            return word;
        }

        public static WORD._Document setWordDocReadingLayoutAllowEditing(WORD._Document word)
        {
            try
            {
                if (word.ActiveWindow.Active)
                {
                    word.ActiveWindow.View.ReadingLayoutAllowEditing = true;
                }
                else
                {
                    log.Warn("WORD's Windows is not Active!");
                }

            }
            catch (Exception ex)
            {
                log.Error("Can't set WORD ReadingLayoutAllowEditing to true (in writable mode)!..." + ex.Message);
            }

            return word;
        }

        public static MsoEncoding GetMsoEncoding(Encoding encodingValue)
        {
            MsoEncoding msoEnc = MsoEncoding.msoEncodingUTF8;
            log.Debug("File encoding: ("+encodingValue.ToString()+"). Setting default MsoEncoding: " + msoEnc.ToString());

            if (encodingValue.Equals(Encoding.ASCII)) msoEnc = MsoEncoding.msoEncodingBaltic;
            if (encodingValue.Equals(Encoding.UTF8)) msoEnc = MsoEncoding.msoEncodingUTF8;
            if (encodingValue.Equals(Encoding.UTF7)) msoEnc = MsoEncoding.msoEncodingUTF7;
            //if (encodingValue.Equals(Encoding.UTF32)) msoEnc = MsoEncoding.;
            if (encodingValue.Equals(Encoding.BigEndianUnicode)) msoEnc = MsoEncoding.msoEncodingUnicodeBigEndian;
            if (encodingValue.Equals(Encoding.Unicode)) msoEnc = MsoEncoding.msoEncodingUTF8;

            log.Debug("Returning MsoEncoding: " + msoEnc.ToString());
            return msoEnc;
        }

        public static object MSOEncoding(String encoding, Encoding enc)
        {
            object msoEnc = oMissing;
            if (enc == null)
            {
                log.Warn("File encoding (NULL), Request input encoding: " + encoding);
                msoEnc = oMissing;
            }
            else
            {
                //log.Debug("File encoding codepage: " + enc.CodePage);

                Encoding encodingValue = encoding != null ? Encoding.GetEncoding(encoding) : null;
                if (encodingValue == null)
                {
                    log.Warn("Request input encoding is NULL! Using file encoding...");
                    encodingValue = enc;
                }
                else
                {
                    if (encodingValue.Equals(enc))
                    {
                        log.Debug("File encoding is equal to request input param: " + enc.ToString());
                        //log.Debug("Request encoding codepage: " + encodingValue.CodePage);
                    }
                    else
                    {
                        log.Warn("File encoding [" + enc.ToString() + "] is different than request input param: [" + encodingValue.ToString() + "]!");
                        //log.Debug("Request encoding codepage: " + encodingValue.CodePage);
                        //encodingValue = enc;
                    }
                }
                msoEnc = GetMsoEncoding(encodingValue);
            }

            return msoEnc;
        }

        public static WORD._Document openWordDocument(WORD._Application word, WORD._Document doc, object filename, object msoEnc)
        {
            int i = 0;
            while (i < 5)
            {
                i++;
                log.Debug(i + ") Attempting to open WORD Document...");

                try
                {
                    log.Debug(i + ") Opening WORD document...");
                    doc = word.Documents.OpenNoRepairDialog(ref filename, false,
                    false, false, ref oMissing, ref oMissing, true,
                    ref oMissing, ref oMissing, ref oMissing, msoEnc, false,
                    false, ref oMissing, true, ref oMissing);

                    log.Debug(i + ") Set Word Document active...");
                    doc.Activate();

                    log.Debug(i + ") Set Word Document params...");
                    doc = MsoUtils.setWordDocWordParams(doc);

                    log.Debug(i + ") WORD Document open!");
                    return doc;
                }
                catch (Exception ex)
                {
                    log.Error(i + ") Can't open WORD Document: ERROR message: " + ex.Message);
                    log.Error(i + ") ERROR StackTrace: " + ex.StackTrace);
                }
                log.Debug(i+ ") Next attempt...");
            }

            log.Debug("Returning WORD Document: NULL!");
            return null;
        }

        public static bool pdfGenerate(string uri, string fileEncoding, string fileType)
        {
            bool pdfGenerateResult = false;
            if ((string.Compare(fileType, "doc")) == 0 || (string.Compare(fileType, "docx")) == 0 || (string.Compare(fileType, "rtf")) == 0)
            {
                log.Debug("Input filetype [" + fileType + "] is non plain text type. Generate pdf by WORD...");
                pdfGenerateResult = PdfUtil.generatePdfWord(uri, fileEncoding, false);
            }
            else if ((string.Compare(fileType, "txt")) == 0 || (string.Compare(fileType, "html")) == 0)
            {
                log.Debug("Input filetype [" + fileType + "] is plain textfile type. Generate pdf by WORD...");
                pdfGenerateResult = PdfUtil.generatePdfWord(uri, fileEncoding, true);
            }
            else if ((string.Compare(fileType, "xls")) == 0 || (string.Compare(fileType, "xlsx")) == 0)
            {
                log.Debug("Input filetype [" + fileType + "] is table type. Generate pdf by EXCEL...");
                pdfGenerateResult = PdfUtil.generatePdfExel(uri, fileType);
            }
            else
            {
                log.Warn("Filetype (" + fileType + ") is not in acceptable format! ");
            }

            Utils.removeOldFile(uri);

            return pdfGenerateResult;
        }
    }
}