﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Microsoft.Office.Interop.Word;
using WORD = Microsoft.Office.Interop.Word;

using log4net;
using Mozilla.CharDet;
using System.Management;
using System.Configuration;
using System.Net;

namespace MSOService
{
    public class Utils
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string PROCESS_WORD = "winword";
        private const string PROCESS_EXCEL = "excel";
        public static object oMissing = Missing.Value;
        //private static bool removeEmptyFieldsParam = true;

        // Method to generate uniq system id
        public static string getGuid()
        {
            String newGuid = Guid.NewGuid().ToString();
            log.Debug("getGuid: Getting new GUID: " + newGuid);
            return newGuid;
        }

        public static Dictionary<String, String> getFileTypeDictionary()
        {
            Dictionary<string, string> types = new Dictionary<string, string>();
            types.Add("application/dot", "doc");
            types.Add("application/msword", "doc");
            types.Add("application/rtf", "rtf");
            types.Add("application/vnd.openxmlformats-officedocument.wordprocessingml.document", "docx");
            types.Add("application/vnd.openxmlformats-officedocument.wordprocessingml.template", "dotx");
            types.Add("text/plain", "txt");
            types.Add("text/html", "html");
            types.Add("application/vnd.excel", "xls");
            types.Add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "xlsx");

            return types;
        }

        public static string getFileTypeFromMimeType(string mimeType)
        {
            try
            {
                Dictionary<string, string> types = getFileTypeDictionary();

                log.Debug("getFileTypeFromMimeType: mimeType is: " + mimeType);

                if (types.ContainsKey(mimeType))
                {
                    String fileExt = types[mimeType];
                    if(fileExt == null)
                    {
                        log.Debug("getFileTypeFromMimeType: mimeType: " + mimeType + ":: File type not in Dictionary!");
                    }
                    else
                    {
                        log.Debug("getFileTypeFromMimeType: mimeType: " + mimeType + ":: file type: " + fileExt);
                    }
                    
                    return fileExt;
                }
            }
            catch (Exception ex)
            {
                log.Error("getFileTypeFromMimeType: Error in getFileTypeFromMimeType() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                return null;
            }
            return null;
        }

        public static string getTimestampToFilename(DateTime now)
        {

            String y = now.Year + "";
            String mo = now.Month + "";
            String d = now.Day + "";

            String h = now.Hour + "";
            String m = now.Minute + "";
            String s = now.Second + "";

            String ms = now.Millisecond + "";


            if (mo.Length == 1)
            {
                mo = "0" + mo;
            }

            if (d.Length == 1)
            {
                d = "0" + d;
            }

            if (h.Length == 1)
            {
                h = "0" + h;
            }

            if (m.Length == 1)
            {
                m = "0" + m;
            }

            if (s.Length == 1)
            {
                s = "0" + s;
            }

            if (ms.Length == 1)
            {
                ms = "00" + ms;
            }
            else if (ms.Length == 2)
            {
                ms = "0" + ms;
            }
            String filenameTime = y + "-" + mo + "-" + d + " " + h + "." + m + "." + s + "," + ms;
            return filenameTime;
        }


        public static Boolean writeRequestToFile(string filename, byte[] filedata)
        {
            Boolean status = false;
            string fileDataConfigPath = ConfigurationManager.AppSettings["fileLoggerdir"].ToString();
            string filepath;
            if (true)
            {
                if(fileDataConfigPath == null)
                {
                    filepath = getDocPath("filedata");
                } else
                {
                    filepath = getDocPath(fileDataConfigPath);
                }
                

                log.Debug("writeRequestToFile: file fullpath: " + filepath + "\\" + filename);

                try
                {
                    File.WriteAllBytes(filepath + "\\" + filename, filedata);
                    /*
                    string extension = Path.GetExtension(@filepath + "\\" + filename);
                    log.Debug("Extension: " + extension);
                    if((extension.ToLower() == ".html") || extension.ToLower() == ".txt")
                    {
                        log.Debug("Find file encoding....");
                        Encoding enc = findFileEncoding(filepath + "\\" + filename, extension);
                    } else
                    {
                        log.Debug("No TEXT type file...");
                    }
                    */
                    status = true;
                }
                catch (Exception ex)
                {
                    //EventLog.WriteEntry(LOGSOURCE, "Error in writeLogRecordToFile() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                    log.Error("writeRequestToFile: ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                }

            }
            return status;
        }

        public static string getDocPath(string path)
        {
            string fullPath = @"";
            try
            {
                DateTime now = DateTime.Now;

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                // Get full path
                fullPath = System.IO.Path.GetFullPath(path);
                if (!Directory.Exists(fullPath + "\\" + now.Year))
                {
                    Directory.CreateDirectory(fullPath + "\\" + now.Year);
                }
                // Get full path + year folder
                fullPath = System.IO.Path.GetFullPath(fullPath + "\\" + now.Year);
                if (!Directory.Exists(fullPath + "\\" + now.Month))
                {
                    Directory.CreateDirectory(fullPath + "\\" + now.Month);
                }
                fullPath = System.IO.Path.GetFullPath(fullPath + "\\" + now.Month);
                if (!Directory.Exists(fullPath + "\\" + now.Day))
                {
                    Directory.CreateDirectory(fullPath + "\\" + now.Day);
                }
                // Get full path: like "log/2014/02/23/
                fullPath = System.IO.Path.GetFullPath(fullPath + "\\" + now.Day);
                if (!Directory.Exists(fullPath + "\\" + now.Hour))
                {
                    Directory.CreateDirectory(fullPath + "\\" + now.Hour);
                }
                // Get full path: like "log/2014/02/23/14
                fullPath = System.IO.Path.GetFullPath(fullPath + "\\" + now.Hour);

            }
            catch (Exception ex)
            {
                //EventLog.WriteEntry(LOGSOURCE, "Error in CreateIfMissing() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                log.Error("getDocPath: Error in CreateIfMissing() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString());
            }
            return fullPath;
        }

        /*
         * Method to generate file location
         * input - filename and file extension as String
         * return - file full path as String
         * */
        public static String generateUri(String fileType, String fileName)
        {
            try
            {
                string location = @"";
                location += Path.GetTempPath();
                string uri = location;
                uri += fileName;
                uri += ".";
                uri += fileType;
                return uri;
            }
            catch (Exception ex)
            {
                //EventLog.WriteEntry(LOGSOURCE, "Error in generateUri() method. " + ex.Message + " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                log.Error("generateUri: ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                return null;
            }
        }

        public static List<Process> GetProcesses(string name)
        {
            return new List<Process>(Process.GetProcessesByName(name));
        }

        public static void FilterProcesses(List<Process> previousProcesses, string name)
        {
            string processesInfo = "";
            log.Debug("FilterProcesses()...");
            foreach (Process process in Process.GetProcessesByName(name))
            {
                log.Debug("Process: ID: " + process.Id + ", Name: " + process.ProcessName);
                bool foundPreviousProcess = false;
                foreach (Process perviousProcess in previousProcesses)
                {
                    if (perviousProcess.Id == process.Id)
                    {
                        log.Debug("Remove previous process (ID: " + perviousProcess + ")");
                        previousProcesses.Remove(perviousProcess);
                        foundPreviousProcess = true;
                        break;
                    }
                }
                if (!foundPreviousProcess)
                {
                    processesInfo += ", " + process.Id + " " + process.ProcessName;
                    previousProcesses.Add(process);
                }
            }
            //EventLog.WriteEntry(LOGSOURCE, "Found possible processes: " + processesInfo, EventLogEntryType.Information);
            log.Info("Found possible processes: " + processesInfo);
        }

        // http://stackoverflow.com/a/2097538/1919076
        public static int findInFileIgnoreCase(string fileName, string value, Encoding encodingValue)
        {   // returns complement of number of characters in file if not found
            // else returns index where value found
            int index = 0;
            using (StreamReader reader = new StreamReader(fileName, encodingValue, false))
            {
                if (String.IsNullOrEmpty(value))
                    return 0;
                StringSearch valueSearch = new StringSearch(value.ToLower());
                int readChar;
                while ((readChar = reader.Read()) >= 0)
                {
                    ++index;
                    if (valueSearch.Found(Char.ToLower((char)readChar)))
                        return index - value.Length;
                }
            }
            return ~index;
        }

        public class StringSearch
        {   // Call Found one character at a time until string found
            private readonly string value;
            private readonly List<int> indexList = new List<int>();
            public StringSearch(string value)
            {
                this.value = value;
            }
            public bool Found(char nextChar)
            {
                for (int index = 0; index < indexList.Count;)
                {
                    int valueIndex = indexList[index];
                    if (value[valueIndex] == nextChar)
                    {
                        ++valueIndex;
                        if (valueIndex == value.Length)
                        {
                            indexList[index] = indexList[indexList.Count - 1];
                            indexList.RemoveAt(indexList.Count - 1);
                            return true;
                        }
                        else
                        {
                            indexList[index] = valueIndex;
                            ++index;
                        }
                    }
                    else
                    {   // next char does not match
                        indexList[index] = indexList[indexList.Count - 1];
                        indexList.RemoveAt(indexList.Count - 1);
                    }
                }
                if (value[0] == nextChar)
                {
                    if (value.Length == 1)
                        return true;
                    indexList.Add(1);
                }
                return false;
            }
            public void Reset()
            {
                indexList.Clear();
            }
        }

        public static void copyText(TextReader reader, TextWriter writer)
        {
            char[] buffer = new char[8192];
            int charsRead;
            while ((charsRead = reader.Read(buffer, 0, buffer.Length)) > 0)
            {
                writer.Write(buffer, 0, charsRead);
            }
        }

        /*
         * Method to find all fields in document
         * 
         * */
        public static List<Field> findFields(_Document doc)
        {
            try
            {
                doc.ActiveWindow.View.ReadingLayout = false;

                List<Field> foundFields = new List<Field>();

                if(doc != null && doc.StoryRanges != null)
                {
                    log.Debug("Find document fields from StoryRanges... count: " + doc.StoryRanges.Count);
                }
                else
                {
                    log.Debug("Find document fields from StoryRanges... count: NULL");
                }

                // -------------------------------------------------------------------
                int j = 0;
                foreach (Range tmpRange in doc.StoryRanges)
                {
                    j++;
                    WdStoryType storyType = tmpRange.StoryType;
                    log.Debug("Range: " + j + ") findFields: story type = " + storyType.ToString());

                    int tmpRangeFieldsCount = tmpRange.Fields.Count;
                    log.Debug("Range: " + j + ") tmpRange Fields count: " + tmpRangeFieldsCount);
                    if (tmpRangeFieldsCount > 0)
                    {
                        int i = 0;
                        foreach (Field field in tmpRange.Fields)
                        {
                            i++;
                            try
                            {
                                foundFields.Add(field);
                                log.Debug("Range: " + j + ") Field: " + i + ") findFields: Field:[" + field.ToString() + "], text: [" + field.Code.Text + "]");

                            } catch(Exception ex)
                            {
                                log.Error("Range: " + j + ") Field: " + i + ") Field reading error: " + ex.Message + "; StackTrace: " + ex.StackTrace);
                            }
                        }
                    }

                    int tmpRangeShapeRangeCount = tmpRange.ShapeRange.Count;
                    log.Debug("Range: " + j + ") tmpRange ShapeRange count: " + tmpRangeShapeRangeCount);
                    if (tmpRangeShapeRangeCount > 0)
                    {
                        int i = 0;
                        foreach (Shape oShp in tmpRange.ShapeRange)
                        {
                            i++;
                            log.Debug("Range: " + j + ") Shape: " + i + ") findFields: oShp = " + oShp.AlternativeText);
                            if (oShp.TextFrame.HasText < 0)
                            {
                                log.Debug("Range: " + j + ") Shape: " + i + ") findFields: has text = " + oShp.TextFrame.HasText.ToString());
                                foreach (Field fld in oShp.TextFrame.TextRange.Fields)
                                {
                                    foundFields.Add(fld);
                                    log.Debug("Range: " + j + ") Shape: " + i + ") findFields: Field add 2 = " + fld.ToString());
                                }
                            }
                        }
                    }



                }
                return foundFields;
            }
            catch (Exception ex)
            {
                log.Error("findFields: ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                return null;
            }
        }

        public static void fixFieldCodes(List<Field> fields)
        {
            int i = 0;
            foreach (Field field in fields)
            {

                i++;
                // " DOCPROPERTY  delta_recipientPersonName.1  \\* MERGEFORMAT "
                string fieldCode = field.Code.Text;
                if (fieldCode == null)
                {
                    log.Debug(i +") FielCode is null: Continue...");
                    continue;
                }
                //Console.WriteLine(i + ") FieldCode: [" + fieldCode+ "]");

                fieldCode = fieldCode.Trim();
                if (fieldCode.ToUpper().StartsWith("DOCPROPERTY "))
                {
                    fieldCode = fieldCode.Substring(12);
                }
                else if (fieldCode.ToUpper().StartsWith("REF "))
                {
                    fieldCode = fieldCode.Substring(4);
                }
                fieldCode = fieldCode.Trim();
                if (!fieldCode.StartsWith("delta_"))
                {
                    continue;
                }
                int endMerge = fieldCode.IndexOf("\\"); // Search for field flags delimiter
                if (endMerge >= 0 && endMerge < 7)
                {
                    continue;
                }
                string after = "";
                if (endMerge >= 0)
                {
                    after = fieldCode.Substring(endMerge);
                    fieldCode = fieldCode.Substring(0, endMerge);
                    //Console.WriteLine(i + ") FieldCode: [" + fieldCode + "]; after: [" + after + "]");
                }
                string fieldCode1 = fieldCode;
                //Console.WriteLine(i + ") getFieldCodeSubstitutedFromDelta: [" + getFieldCodeSubstitutedFromDelta(fieldCode1) + "]");

                field.Code.Text = " " + getFieldCodeSubstitutedFromDelta(fieldCode) + after;
                log.Debug(i + ") Field Code Text: " + field.Code.Text);
            }
        }

        public static string getFieldCodeSubstitutedFromDelta(string fieldCode)
        {
            string fieldStr = fieldCode.Replace("$", "\"").Replace(".", "_");
            log.Debug("Fix field: " + fieldCode + " ==> " + fieldStr);
            return fieldStr;
        }

        public static string getFieldCodeSubstitutedBackToDelta(string fieldCode)
        {
            return fieldCode.Replace("\"", "$").Replace("_", ".");
        }

        public static string getDeltaFieldCode(string fieldCode)
        {
            // " delta_recipientPersonName.1  \\* MERGEFORMAT "
            if (fieldCode == null)
            {
                return null;
            }
            fieldCode = fieldCode.Trim();
            if (!fieldCode.StartsWith("delta_"))
            {
                return null;
            }
            int endMerge = fieldCode.IndexOf("\\"); // Search for field flags delimiter
            if (endMerge >= 0 && endMerge < 7)
            {
                return null;
            }
            string deltaFieldCode = endMerge < 0 ? fieldCode.Substring(6) : fieldCode.Substring(6, endMerge - 6); // If there are no flags, the remaining text is property name
            deltaFieldCode = deltaFieldCode.Trim();
            return deltaFieldCode;
        }

        public static string getDeltaFieldResult(string deltaFieldCode, string fieldResult)
        {
            string deltaFieldResult = fieldResult;
            if (deltaFieldResult == null || (deltaFieldResult.StartsWith("{") && deltaFieldResult.EndsWith("}")))
            {
                deltaFieldResult = "";
            }
            return deltaFieldResult;
        }

        public static void KillProcesses(List<Process> processes)
        {
            foreach (Process process in processes)
            {
                if (!process.HasExited)
                {
                    try
                    {
                        process.Kill();
                        process.WaitForExit(); // use a timeout?
                        //EventLog.WriteEntry(LOGSOURCE, "Process " + process.Id + " " + process.ProcessName + " terminated", EventLogEntryType.Warning);
                        log.Warn("Process " + process.Id + " " + process.ProcessName + " terminated");

                    }
                    catch (Exception ex)
                    {
                        //EventLog.WriteEntry(LOGSOURCE, "Error terminating process " + process.Id + " " + process.ProcessName + " . " + ex.Message + " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                        log.Error("Error terminating process " + process.Id + " " + process.ProcessName + " . " + ex.Message + " Stack: " + ex.StackTrace.ToString());
                    }
                }
            }
        }


        /*
         * Regex in finalizeSearch method does not match, if a field is right next to the phrase we want to match (comment phrase in our case), without any character between them.
         * So, insert extra spaces to work around that issue.
         * */
        public static bool insertExtraSpaces(_Document doc)
        {
            bool documentChanged = false;
            try
            {
                doc = MsoUtils.setWordDocWordParams(doc);

                _Application app = doc.StoryRanges.Application;

                app = MsoUtils.setWordAppWordParams(app);

                List<Field> list = new List<Field>();
                foreach (Field singleField in app.Selection.Fields)
                {
                    list.Add(singleField);
                }
                foreach (Field singleField in list)
                {
                    try
                    {
                        singleField.Select();
                        app.Selection.Collapse(WdCollapseDirection.wdCollapseStart);
                        Range range = app.Selection.Range;
                        range.MoveStart(WdUnits.wdCharacter, -2);
                        if (string.Compare(range.Text, "/*") == 0)
                        {
                            range.InsertAfter(" ");
                            documentChanged = true;
                        }

                        singleField.Select();
                        app.Selection.Collapse(WdCollapseDirection.wdCollapseEnd);
                        range = app.Selection.Range;
                        range.MoveEnd(WdUnits.wdCharacter, 2);
                        if (string.Compare(range.Text, "*/") == 0)
                        {
                            range.InsertBefore(" ");
                            documentChanged = true;
                        }
                    }
                    catch { }//Kui tekib viga nt. on dokumendis tühi field ilma sisuta
                }
            }
            catch (Exception ex)
            {
                //EventLog.WriteEntry(LOGSOURCE, "Error in method insertExtraSpaces " + ex.Message + " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                log.Error("Error in method insertExtraSpaces " + ex.Message + " Stack: " + ex.StackTrace.ToString());
            }

            return documentChanged;
        }

        public static bool finalizeSearch(_Document doc)
        {
            log.Debug("Finalize search in document text...");
            bool documentChanged = false;
            try
            {
                doc = MsoUtils.setWordDocWordParams(doc);

                _Application app = doc.StoryRanges.Application;

                app = MsoUtils.setWordAppWordParams(app);

                object regex = @"/[\*]*[\*]/";
                app.Selection.Find.ClearFormatting();
                app.Selection.Select();
                app.Selection.Find.Wrap = WdFindWrap.wdFindContinue;
                app.Selection.Find.MatchWildcards = true;

                while (app.Selection.Find.Execute(ref regex,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                    ref oMissing, ref oMissing) == true)
                {
                    log.Debug("While selection find loop... REGEX: " + regex.ToString());
                    bool atLeastOneDeltaFieldHasValue = false;
                    int i = 0;
                    if(app != null && app.Selection != null && app.Selection.Fields != null)
                    {
                        log.Debug("Found " + app.Selection.Fields.Count + " fields!");
                    } else
                    {
                        log.Debug("Found 0 fields!");
                    }
                    foreach (Field field in app.Selection.Fields)
                    {
                        i++;
                        log.Debug(i + ") Field: " + field.Code.Text);
                        string deltaFieldCode = getDeltaFieldCode(field.Code.Text);
                        if (deltaFieldCode == null)
                        {
                            log.Debug(i + ") FieldCodeText is NULL! Continue...");
                            continue;
                        }
                        string deltaFieldResult = getDeltaFieldResult(deltaFieldCode, field.Result.Text);

                        if (deltaFieldResult.Length > 0)
                        {
                            log.Debug("Field result text length is bigger than 0! Length: " + deltaFieldResult.Length + "; Break loop!");
                            atLeastOneDeltaFieldHasValue = true;
                            break;
                        }
                    }

                    if (!atLeastOneDeltaFieldHasValue)
                    {
                        log.Debug("Set selection text to empty string!");
                        app.Selection.Text = "";
                    }
                    else
                    {
                        log.Debug("Remove from selection text 2 characters from first and from last.." + app.Selection.Characters.ToString());

                        app.Selection.Characters.Last.Delete();
                        app.Selection.Characters.Last.Delete();
                        app.Selection.Characters.First.Delete();
                        app.Selection.Characters.First.Delete();
                    }
                    documentChanged = true;
                }
            }
            catch (Exception ex)
            {
                //EventLog.WriteEntry(LOGSOURCE, "Error in method finalizeSearch " + ex.Message + " Stack: " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                log.Error("Error in method finalizeSearch " + ex.Message + " Stack: " + ex.StackTrace.ToString());
            }
            return documentChanged;
        }

        public static bool deleteUnusedFields(Fields fields)
        {
            log.Debug("Delete unused fields...");
            bool documentChanged = false;
            foreach (Field field in fields)
            {
                string deltaFieldCode = getDeltaFieldCode(field.Code.Text);
                if (deltaFieldCode == null)
                {
                    continue;
                }
                string deltaFieldResult = getDeltaFieldResult(deltaFieldCode, field.Result.Text);

                if (deltaFieldResult.Length == 0)
                {
                    log.Debug("Delete empty field: " + field.Code.Text);
                    field.Delete();
                    documentChanged = true;
                }
            }
            return documentChanged;
        }

        public static string dateTimeToString(DateTime dt)
        {
            String strDate = dt.Year + "-" + dt.Month + "-" + dt.Day;
            String strTime = dt.Hour + ":" + dt.Minute + ":" + dt.Second + "," + dt.Millisecond;
            return strDate + " " + strTime;
        }

        public static string convertTimeSpanToHumanRedable(TimeSpan time)
        {
            return time.Days + "d " + time.Hours + "h " + time.Minutes + "m " + time.Seconds + "s";
        }

        public static void removeOldFile(String uri)
        {
            try
            {
                log.Debug("Remove old file by uri: " + uri);
                File.Delete(uri);
            }
            catch (Exception ex)
            {
                //EventLog.WriteEntry(LOGSOURCE, "Unable to delete file with uri " + uri, EventLogEntryType.Error);
                log.Error("Unable to delete file with uri " + uri + "; ERROR: " + ex.Message + " Stack: " + ex.StackTrace.ToString());
            }
        }

        public static Encoding findFileEncoding(string path, string extension)
        {
            Encoding enc = null;
            log.Debug("File extension: " + extension);
            if (((string.Compare(extension, ".txt")) == 0) || ((string.Compare(extension, ".html")) == 0))
            {
                log.Debug("File extention is .TXT or .HTML type. Get encoding...");
                //System.IO.FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);

                byte[] fileBytes = File.ReadAllBytes(path);
                UniversalDetector charDet = new UniversalDetector();

                try
                {
                    charDet.HandleData(fileBytes);
                    string detectedCharset = charDet.DetectedCharsetName;
                    if (detectedCharset != null)
                    {
                        log.Debug("Charset detected: " + detectedCharset);
                        charDet.Reset();

                    }
                    else
                    {
                        log.Debug("Charset not found! NULL");
                    }
                    /*
                    if (file.CanSeek)
                    {
                        byte[] bom = new byte[4]; // Get the byte-order mark, if there is one 
                        file.Read(bom, 0, 4);
                        //log.Debug("File BOM: " + bom.ToString());
                        String byteStr = "";
                        foreach(Byte b in bom)
                        {
                            byteStr += b.ToString() + ";";
                            
                        }
                        log.Debug("BOM Byte: " + byteStr);

                        if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf)
                        {
                            log.Debug("1) File is UNICODE (UTF-8 BOM) format...");
                        }
                        if (bom[0] == 0xff && bom[1] == 0xfe)
                        {
                            log.Debug("1) File is UNICODE (ucs-2le, ucs-4le, ucs-16le) format...");
                        }
                        if (bom[0] == 0xfe && bom[1] == 0xff)
                        {
                            log.Debug("1) File is UNICODE (utf-16, ucs-2) format...");
                        }
                        if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff)
                        {
                            log.Debug("1) File is UNICODE (ucs-4) format...");
                        }

                            // UTF-8 With BOM
                        if (bom[0] == 239 && bom[1] == 187 && bom[2] == 192)
                        {
                            log.Debug("2) File is (UTF-8 BOM) format...");
                            enc = System.Text.Encoding.Unicode;
                            // UCS-2 LE BOM
                        }
                        if (bom[0] == 255 && bom[1] == 254 && bom[2] == 246)
                        {
                            log.Debug("2) File is (UCS-2 LE BOM) format...");
                            enc = System.Text.Encoding.Unicode;
                        }
                        // ANSI
                        if (bom[0] == 246 && bom[1] == 228 && bom[2] == 252)
                        {
                            log.Debug("2) File is (ANSI (ASCII)) format...");
                            enc = System.Text.Encoding.ASCII;
                        }
                        
                        if ((bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf) || // utf-8 
                            (bom[0] == 0xff && bom[1] == 0xfe) || // ucs-2le, ucs-4le, and ucs-16le 
                            (bom[0] == 0xfe && bom[1] == 0xff) || // utf-16 and ucs-2 
                            (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff)) // ucs-4 
                        {
                            log.Debug("File is unicode format...");
                            enc = System.Text.Encoding.Unicode;
                        }
                        else
                        {
                            log.Debug("Set file encoding to ASCII, because it is not in UNICODE!");
                            enc = System.Text.Encoding.ASCII;
                        }
                        

                        // Now reposition the file cursor back to the start of the file 
                        file.Seek(0, System.IO.SeekOrigin.Begin);
                    }
                
                    else
                    {
                        log.Debug("Can't seek file. Set encoding to ASCII!");
                        enc = System.Text.Encoding.ASCII;
                    }
                */
                }
                finally
                {
                    //file.Close();
                }

                log.Debug("Encoding: " + enc.ToString());
            } else
            {
                log.Warn("File is not TEXT type! Encoding is NULL.");
            }

            return enc;
        }
        public static Encoding GetEncoding(string filename)
        {
            // Read the BOM
            var bom = new byte[4];
            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                file.Read(bom, 0, 4);
            }

            // Analyze the BOM
            if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76) return Encoding.UTF7;
            if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf) return Encoding.UTF8;
            if (bom[0] == 0xff && bom[1] == 0xfe) return Encoding.Unicode; //UTF-16LE
            if (bom[0] == 0xfe && bom[1] == 0xff) return Encoding.BigEndianUnicode; //UTF-16BE
            if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff) return Encoding.UTF32;
            return Encoding.ASCII;
        }

        public static void checkHtmlContent(string path, string ext, Encoding enc)
        {
            bool writeHtmlTags = false;

            if (string.Compare(ext, ".html") == 0)
            {
                log.Debug("Filetype is HTML. Try find <HTML>-tag...");
                writeHtmlTags = findInFileIgnoreCase(path, "<html", enc) < 0;
                if (writeHtmlTags)
                {
                    log.Warn("... <HTML>-tag not found!");
                }
            }

            if (writeHtmlTags || ((string.Compare(ext, ".txt") == 0 || string.Compare(ext, ".html") == 0) && !Encoding.UTF8.Equals(enc)))
            {
                StreamWriter outWriter = new StreamWriter(path + "_", false, Encoding.UTF8);
                StreamReader inReader = new StreamReader(path, enc, false);
                try
                {
                    if (writeHtmlTags)
                    {
                        outWriter.Write("<html>");
                    }

                    copyText(inReader, outWriter);

                    if (writeHtmlTags)
                    {
                        outWriter.Write("</html>");
                    }
                }
                finally
                {
                    inReader.Close();
                    outWriter.Close();
                }
                File.Delete(path);
                File.Move(path + "_", path);
            }
        }

        public static void checkSystemFolders()
        {
            try
            {
                string path1 = "C:\\Windows\\SysWOW64\\config\\systemprofile\\Desktop";
                log.Debug("check system path1: " + path1);
                DirectoryInfo dir1 = Directory.CreateDirectory(@path1);
                log.Debug("check system path1: " + path1 + "... " + dir1.Exists);

                string path2 = "C:\\Windows\\System32\\config\\systemprofile\\Desktop";
                log.Debug("check system path2: " + path2);
                DirectoryInfo dir2 = Directory.CreateDirectory(@path2);
                log.Debug("check system path1: " + path2 + "... " + dir2.Exists);
            } catch(Exception ex)
            {
                log.Error("Can't check/create system directory: " + ex.Message, ex);
            }
        }

        public static bool checkPrinter()
        {
            bool defaultPrinterExists = false;
            try
            {
                // Set management scope
                ManagementScope scope = new ManagementScope(@"\root\cimv2");
                scope.Connect();

                // Select Printers from WMI Object Collections
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer");

                if(searcher != null && searcher.Get() != null)
                {
                    foreach (ManagementObject printer in searcher.Get())
                    {
                        log.Debug("Printer FOUND! - " + printer["Name"] + "; Spooler status: " + printer["SpoolEnabled"].ToString());
                        if (printer["Default"].ToString().ToLower().Equals("true"))
                        {
                            log.Debug("Default pinter found! ");
                            defaultPrinterExists = true;
                        }
                        else
                        {
                            log.Debug("This printer is not default...");
                        }
                    }
                }
                else
                {
                    log.Debug("---------------------------------------------------------------------");
                    log.Debug("NO PRINTERS FOUND! EXCEL PROCCESS NEED'S DEFAULT PRINTER!");
                    log.Debug("---------------------------------------------------------------------");
                    return false;
                }
            } catch(Exception ex)
            {
                log.Debug("---------------------------------------------------------------------");
                log.Error("Printer check failed: " + ex.Message, ex);
                log.Debug("---------------------------------------------------------------------");
                return false;
            }

            return defaultPrinterExists;

        }

        public static void serverCertificateValidation()
        {
            log.Debug("Set server certificate validation callback true..");
            try
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => { return true; };
            }
            catch (Exception ex)
            {
                log.Error("Can't set certificate validation always TRUE! " + ex.Message);
            }
        }
    }
}