﻿namespace MsoServices
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MSOServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.MSOServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // MSOServiceProcessInstaller
            // 
            this.MSOServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.MSOServiceProcessInstaller.Password = null;
            this.MSOServiceProcessInstaller.Username = null;
            // 
            // MSOServiceInstaller
            // 
            this.MSOServiceInstaller.ServiceName = "MSOServiceHandler";
            this.MSOServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.MSOServiceProcessInstaller,
            this.MSOServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller MSOServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller MSOServiceInstaller;
    }
}